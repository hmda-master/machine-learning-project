import os

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.inspection import DecisionBoundaryDisplay
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score, roc_curve, ConfusionMatrixDisplay, classification_report, confusion_matrix, \
    precision_recall_fscore_support, accuracy_score
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import seaborn as sns


def split_data(x_, y_):
    '''
     indica que el 30% de tus datos se reservará para el conjunto de prueba, mientras que el 70% se utilizará para
      el conjunto de entrenamiento. El argumento random_state=42 se utiliza para establecer una semilla para la generación
      de números aleatorios, lo que garantiza que la división sea reproducible.
    '''
    num_datos_total = x_.shape[0]

    # Imprimir el número total de datos
    print("Número total de datos:", num_datos_total)
    X_train, X_test, y_train, y_test = train_test_split(x_, y_, test_size=0.3, random_state=42)
    print("Dimensiones de X_train:", X_train.shape)
    print("Dimensiones de y_train:", y_train.shape)
    print("Número de muestras en el conjunto de entrenamiento:", X_train.shape[0])
    print("Número de muestras en el conjunto de prueba:", y_test.shape[0])
    return X_train, X_test, y_train, y_test


def bar_diagram(df, x_label, y_label, title, method, type_method):
    ruta_carpeta = crear_carpeta_resultados('feature-selection/' + method + '_' + type_method)
    nombre_archivo = "bar_diagram.png"
    ruta_archivo = os.path.join(ruta_carpeta, nombre_archivo)
    plt.figure(figsize=(10, 6))
    plt.barh(df[x_label], df[y_label], color='skyblue')
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.gca().invert_yaxis()
    plt.yticks(df[x_label], fontsize=10)
    plt.gca().yaxis.set_major_locator(plt.MultipleLocator(base=2.0))
    plt.subplots_adjust(left=0.25)
    plt.savefig(ruta_archivo)
    plt.close()


def roc_curve_plt_logistRegression(features, labels, chi2_df, method, type_method):
    ruta_carpeta = crear_carpeta_resultados('feature-selection/' + method + '_' + type_method)
    nombre_archivo = "roc_curve.png"
    ruta_archivo = os.path.join(ruta_carpeta, nombre_archivo)

    X_train, X_test, y_train, y_test = train_test_split(features[chi2_df['Feature']], labels, test_size=0.3,
                                                        random_state=42)

    model = LogisticRegression(max_iter=2000)
    model.fit(X_train, y_train)
    y_pred = model.predict_proba(X_test)[:, 1]
    c_index = roc_auc_score(y_test, y_pred)
    fpr, tpr, _ = roc_curve(y_test, y_pred)
    plt.figure(figsize=(8, 6))
    plt.plot(fpr, tpr, color='darkorange', lw=2, label=f'AUC = {c_index:.2f}')
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate (FPR)')
    plt.ylabel('True Positive Rate (TPR)')
    plt.title('Receiver Operating Characteristic (ROC) Curve')
    plt.legend(loc='lower right')
    plt.savefig(ruta_archivo)
    plt.close()
    print("C-index (AUC-ROC) después de la selección de características para " + method + type_method + ":", c_index)

    return c_index


def roc_curve_plt_model(model, X_test, y_test):
    y_pred = model.predict_proba(X_test)[:, 1]
    c_index = roc_auc_score(y_test, y_pred)
    fpr, tpr, _ = roc_curve(y_test, y_pred)
    plt.figure(figsize=(8, 6))
    plt.plot(fpr, tpr, color='darkorange', lw=2, label=f'AUC = {c_index:.2f}')
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate (FPR)')
    plt.ylabel('True Positive Rate (TPR)')
    plt.title('Receiver Operating Characteristic (ROC) Curve')
    plt.legend(loc='lower right')
    plt.show()
    print("C-index (AUC-ROC) después de la selección de características para :", c_index)

    return c_index


def confusion_matrix_plot(model, test_data, test_label, title, method):
    model_predict = model.predict(test_data)
    label_mapping = {0: "LIVING", 1: "DECEASED"}
    etiquetas_reales = [label_mapping[label] for label in test_label]
    etiquetas_predichas = [label_mapping[label] for label in model_predict]
    cm = confusion_matrix(etiquetas_reales, etiquetas_predichas)
    make_confusion_matrix(cm, sum_stats=False)  # function to make it pretty
    print("Matriz de Confusión:\n", cm)
    cm_display = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=label_mapping.values())
    # Graficar la matriz de confusión

    ruta_carpeta = crear_carpeta_resultados(method + '/' + title)
    nombre_archivo = "confusion_matrix_plot.png"
    ruta_archivo = os.path.join(ruta_carpeta, nombre_archivo)

    cm_display.plot(cmap=plt.cm.Blues)
    plt.title("Matriz de Confusión " + title)
    plt.xlabel("Etiqueta Predicha")
    plt.ylabel("Etiqueta Real")
    plt.savefig(ruta_archivo)
    plt.close()
    classification_report(test_label, model_predict)

    precision, recall, f1, support = precision_recall_fscore_support(etiquetas_reales, etiquetas_predichas,
                                                                     average='macro')
    accuracy = accuracy_score(etiquetas_reales, etiquetas_predichas)
    # y_pred = model.predict_proba(test_data)[:, 1]
    c_index = roc_auc_score(test_label, model_predict)
    metrics = {
        'Precision': precision,
        'Recall': recall,
        'F1-score': f1,
        'Support': support,
        'Accuracy': accuracy,
        'c-index': c_index
    }

    return metrics


def decision_boundary_plot(estimator, X, y, model='model', ax=None, method='model'):
    ruta_carpeta = crear_carpeta_resultados(method + '/' + model)
    nombre_archivo = "decision_boundary.png"
    ruta_archivo = os.path.join(ruta_carpeta, nombre_archivo)
    _X = PCA(n_components=2).fit_transform(X, y)
    _X = pd.DataFrame(_X)

    decision_boundary = DecisionBoundaryDisplay.from_estimator(
        estimator.fit(_X, y),
        _X,
        response_method='predict',
        cmap=plt.cm.coolwarm,
        alpha=0.8,
        ax=ax
    )
    decision_boundary.ax_.scatter(
        x=_X[0],
        y=_X[1],
        c=y,
        cmap=plt.cm.coolwarm,
        s=20,
        edgecolors='k'
    )
    if ax:
        ax.set_title(f'Decision boundary of {model}')
    else:
        plt.title(f'Decision boundary of {model}')

    # plt.show()
    plt.savefig(ruta_archivo)
    plt.close()


def tree_structure(tree, features_names=None):
    n_nodes = tree.tree_.node_count
    children_left = tree.tree_.children_left
    children_right = tree.tree_.children_right
    feature = tree.tree_.feature
    threshold = tree.tree_.threshold

    if features_names is not None:
        feature = features_names[feature]

    node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
    is_leaves = np.zeros(shape=n_nodes, dtype=bool)
    stack = [(0, 0)]  # start with the root node id (0) and its depth (0)
    while len(stack) > 0:
        # `pop` ensures each node is only visited once
        node_id, depth = stack.pop()
        node_depth[node_id] = depth

        # If the left and right child of a node is not the same we have a split
        # node
        is_split_node = children_left[node_id] != children_right[node_id]
        # If a split node, append left and right children and depth to `stack`
        # so we can loop through them
        if is_split_node:
            stack.append((children_left[node_id], depth + 1))
            stack.append((children_right[node_id], depth + 1))
        else:
            is_leaves[node_id] = True

    print(
        "The binary tree structure has {n} nodes and has "
        "the following tree structure:\n".format(n=n_nodes)
    )
    for i in range(n_nodes):
        if is_leaves[i]:
            print(
                "{space}node={node} is a leaf node.".format(
                    space=node_depth[i] * "\t", node=i
                )
            )
        else:
            print(
                "{space}node={node} is a split node: "
                "go to node {left} if X[:, {feature}] <= {threshold} "
                "else to node {right}.".format(
                    space=node_depth[i] * "\t",
                    node=i,
                    left=children_left[i],
                    feature=feature[i],
                    threshold=threshold[i],
                    right=children_right[i],
                )
            )


def plot_svc_decision_boundary(svm_clf, xmin, xmax):
    w = svm_clf.coef_[0]
    b = svm_clf.intercept_[0]

    # At the decision boundary, w0*x0 + w1*x1 + b = 0
    # => x1 = -w0/w1 * x0 - b/w1
    x0 = np.linspace(xmin, xmax, 200)
    decision_boundary = -w[0] / w[1] * x0 - b / w[1]

    margin = 1 / w[1]
    gutter_up = decision_boundary + margin
    gutter_down = decision_boundary - margin

    svs = svm_clf.support_vectors_
    plt.style.use('seaborn-whitegrid')  # Cambia el estilo de fondo a blanco con cuadrícula
    plt.scatter(svs[:, 0], svs[:, 1], s=180, facecolors='#FFAAAA')
    plt.plot(x0, decision_boundary, "k-", linewidth=2)
    plt.plot(x0, gutter_up, "k--", linewidth=2)
    plt.plot(x0, gutter_down, "k--", linewidth=2)


def plot_3D_decision_function(X, y, ax, w, b, x1_lim=[-2, 2.5], x2_lim=[-2, 2.5]):
    x1_in_bounds = (X[:, 0] > x1_lim[0]) & (X[:, 0] < x1_lim[1])
    X_crop = X[x1_in_bounds]
    y_crop = y[x1_in_bounds]
    x1s = np.linspace(x1_lim[0], x1_lim[1], 20)
    x2s = np.linspace(x2_lim[0], x2_lim[1], 20)
    x1, x2 = np.meshgrid(x1s, x2s)
    xs = np.c_[x1.ravel(), x2.ravel()]
    df = (xs.dot(w) + b).reshape(x1.shape)
    m = 1 / np.linalg.norm(w)
    boundary_x2s = -x1s * (w[0] / w[1]) - b / w[1]
    margin_x2s_1 = -x1s * (w[0] / w[1]) - (b - 1) / w[1]
    margin_x2s_2 = -x1s * (w[0] / w[1]) - (b + 1) / w[1]
    ax.plot_surface(x1s, x2, np.zeros_like(x1),
                    color="b", alpha=0.2, cstride=100, rstride=100)
    ax.plot(x1s, boundary_x2s, 0, "k-", linewidth=2, label=r"$h=0$")
    ax.plot(x1s, margin_x2s_1, 0, "k--", linewidth=2, label=r"$h=\pm 1$")
    ax.plot(x1s, margin_x2s_2, 0, "k--", linewidth=2)
    ax.plot(X_crop[:, 0][y_crop == 1], X_crop[:, 1][y_crop == 1], 0, "g^")
    ax.plot_wireframe(x1, x2, df, alpha=0.3, color="k")
    ax.plot(X_crop[:, 0][y_crop == 0], X_crop[:, 1][y_crop == 0], 0, "bs")
    ax.axis(x1_lim + x2_lim)
    ax.text(4.5, 2.5, 3.8, "Decision function $h$", fontsize=16)
    ax.set_xlabel(r"Petal length", fontsize=16, labelpad=10)
    ax.set_ylabel(r"Petal width", fontsize=16, labelpad=10)
    ax.set_zlabel(r"$h = \mathbf{w}^T \mathbf{x} + b$", fontsize=18, labelpad=5)
    ax.legend(loc="upper left", fontsize=16)


def plot_dataset(X, y, axes):
    plt.plot(X[:, 0][y == 0], X[:, 1][y == 0], "bs")
    plt.plot(X[:, 0][y == 1], X[:, 1][y == 1], "g^")
    plt.axis(axes)
    plt.grid(True, which='both')
    plt.xlabel(r"$x_1$", fontsize=20)
    plt.ylabel(r"$x_2$", fontsize=20, rotation=0)


def plot_predictions(clf, axes):
    x0s = np.linspace(axes[0], axes[1], 100)
    x1s = np.linspace(axes[2], axes[3], 100)
    x0, x1 = np.meshgrid(x0s, x1s)
    X = np.c_[x0.ravel(), x1.ravel()]
    y_pred = clf.predict(X).reshape(x0.shape)
    y_decision = clf.decision_function(X).reshape(x0.shape)
    plt.contourf(x0, x1, y_pred, cmap=plt.cm.brg, alpha=0.2)
    plt.contourf(x0, x1, y_decision, cmap=plt.cm.brg, alpha=0.1)


def print_feature_selection_results(results):
    print("Select Features Univariate")
    print("{:<20} {:<10} {:<20}".format("Model", "C-Index", "Selected Features"))

    for result in results:
        method = result['Method']
        c_index = result['C-Index']
        selected_features = len(result['Selected Features'])
        print("{:<20} {:.2f} {:<20}".format(method, c_index, selected_features))

    print("\n")


def crear_carpeta_resultados(nombre_carpeta):
    # Ruta completa de la carpeta de resultados
    ruta_resultados = os.path.join("results", nombre_carpeta)

    # Comprobar si la carpeta ya existe
    if not os.path.exists(ruta_resultados):
        # Si no existe, crear la carpeta
        os.makedirs(ruta_resultados)

    return ruta_resultados


# This function is copied from https://github.com/DTrimarchi10/confusion_matrix
def make_confusion_matrix(cf,
                          group_names=None,
                          categories='auto',
                          count=True,
                          percent=True,
                          cbar=True,
                          xyticks=True,
                          xyplotlabels=True,
                          sum_stats=True,
                          figsize=None,
                          cmap='Blues',
                          title=None):
    '''
    This function will make a pretty plot of an sklearn Confusion Matrix cm using a Seaborn heatmap visualization.
    Arguments
    ---------
    cf:            confusion matrix to be passed in
    group_names:   List of strings that represent the labels row by row to be shown in each square.
    categories:    List of strings containing the categories to be displayed on the x,y axis. Default is 'auto'
    count:         If True, show the raw number in the confusion matrix. Default is True.
    normalize:     If True, show the proportions for each category. Default is True.
    cbar:          If True, show the color bar. The cbar values are based off the values in the confusion matrix.
                   Default is True.
    xyticks:       If True, show x and y ticks. Default is True.
    xyplotlabels:  If True, show 'True Label' and 'Predicted Label' on the figure. Default is True.
    sum_stats:     If True, display summary statistics below the figure. Default is True.
    figsize:       Tuple representing the figure size. Default will be the matplotlib rcParams value.
    cmap:          Colormap of the values displayed from matplotlib.pyplot.cm. Default is 'Blues'
                   See http://matplotlib.org/examples/color/colormaps_reference.html

    title:         Title for the heatmap. Default is None.
    '''

    # CODE TO GENERATE TEXT INSIDE EACH SQUARE
    blanks = ['' for i in range(cf.size)]

    if group_names and len(group_names) == cf.size:
        group_labels = ["{}\n".format(value) for value in group_names]
    else:
        group_labels = blanks

    if count:
        group_counts = ["{0:0.0f}\n".format(value) for value in cf.flatten()]
    else:
        group_counts = blanks

    if percent:
        group_percentages = ["{0:.2%}".format(value) for value in cf.flatten() / np.sum(cf)]
    else:
        group_percentages = blanks

    box_labels = [f"{v1}{v2}{v3}".strip() for v1, v2, v3 in zip(group_labels, group_counts, group_percentages)]
    box_labels = np.asarray(box_labels).reshape(cf.shape[0], cf.shape[1])

    # CODE TO GENERATE SUMMARY STATISTICS & TEXT FOR SUMMARY STATS
    if sum_stats:
        # Accuracy is sum of diagonal divided by total observations
        accuracy = np.trace(cf) / float(np.sum(cf))

        # if it is a binary confusion matrix, show some more stats
        if len(cf) == 2:
            # Metrics for Binary Confusion Matrices
            precision = cf[1, 1] / sum(cf[:, 1])
            recall = cf[1, 1] / sum(cf[1, :])
            f1_score = 2 * precision * recall / (precision + recall)
            stats_text = "\n\nAccuracy={:0.3f}\nPrecision={:0.3f}\nRecall={:0.3f}\nF1 Score={:0.3f}".format(
                accuracy, precision, recall, f1_score)
        else:
            stats_text = "\n\nAccuracy={:0.3f}".format(accuracy)
    else:
        stats_text = ""

    # SET FIGURE PARAMETERS ACCORDING TO OTHER ARGUMENTS
    if figsize == None:
        # Get default figure size if not set
        figsize = plt.rcParams.get('figure.figsize')

    if xyticks == False:
        # Do not show categories if xyticks is False
        categories = False

    # MAKE THE HEATMAP VISUALIZATION
    plt.figure(figsize=figsize)
    sns.heatmap(cf, annot=box_labels, fmt="", cmap=cmap, cbar=cbar, xticklabels=categories, yticklabels=categories)

    if xyplotlabels:
        plt.ylabel('True label')
        plt.xlabel('Predicted label' + stats_text)
    else:
        plt.xlabel(stats_text)

    if title:
        plt.title(title)

    plt.show()