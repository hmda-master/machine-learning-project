import numpy as np
from matplotlib import pyplot as plt
from sklearn.ensemble import RandomForestClassifier, VotingClassifier, BaggingClassifier, StackingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier


def plot_decision_boundary(clf, X, y, alpha=1.0):
    axes = [-1.5, 2.4, -1, 1.5]
    x1, x2 = np.meshgrid(np.linspace(axes[0], axes[1], 100),
                         np.linspace(axes[2], axes[3], 100))
    X_new = np.c_[x1.ravel(), x2.ravel()]
    y_pred = clf.predict(X_new).reshape(x1.shape)

    plt.contourf(x1, x2, y_pred, alpha=0.3 * alpha, cmap='Wistia')
    plt.contour(x1, x2, y_pred, cmap="Greys", alpha=0.8 * alpha)
    colors = ["#78785c", "#c47b27"]
    markers = ("o", "^")
    for idx in (0, 1):
        plt.plot(X[:, 0][y == idx], X[:, 1][y == idx],
                 color=colors[idx], marker=markers[idx], linestyle="none")
    plt.axis(axes)
    plt.xlabel(r"$x_1$")
    plt.ylabel(r"$x_2$", rotation=0)


def voting_classifier(features, labels):
    X_train, X_test, y_train, y_test = train_test_split(features, labels, random_state=42)

    voting_clf = VotingClassifier(
        estimators=[
            ('lr', LogisticRegression(random_state=42)),
            ('rf', RandomForestClassifier(random_state=42)),
            ('svc', SVC(random_state=42))
        ]
    )
    voting_clf.fit(X_train, y_train)

    for name, clf in voting_clf.named_estimators_.items():
        print(name, "=", clf.score(X_test, y_test))

    voting_clf.predict(X_test[:1])
    [clf.predict(X_test[:1]) for clf in voting_clf.estimators_]
    print(voting_clf.score(X_test, y_test))
    print('Now lets use soft voting:')
    voting_clf.voting = "soft"
    voting_clf.named_estimators["svc"].probability = True
    voting_clf.fit(X_train, y_train)
    voting_clf.score(X_test, y_test)
    print(voting_clf.score(X_test, y_test))


def bagging_pasting(features, labels):
    X_train, X_test, y_train, y_test = train_test_split(features, labels, random_state=42)

    bag_clf = BaggingClassifier(DecisionTreeClassifier(), n_estimators=500,
                                max_samples=100, n_jobs=-1, random_state=42)
    bag_clf.fit(X_train, y_train)

    # print(bag_clf.oob_score_)
    # print(bag_clf.oob_decision_function_[:3])
    y_pred = bag_clf.predict(X_test)
    acc = accuracy_score(y_test, y_pred)
    print(acc)
    # tree_clf = DecisionTreeClassifier(random_state=42)
    # tree_clf.fit(X_train, y_train)


def meta_classifier_random_forest(features, labels):
    X_train, X_test, y_train, y_test = train_test_split(features, labels, random_state=42)
    rnd_clf = RandomForestClassifier(n_estimators=500, max_leaf_nodes=16,
                                     n_jobs=-1, random_state=42)
    rnd_clf.fit(X_train, y_train)
    y_pred_rf = rnd_clf.predict(X_test)

    bag_clf = BaggingClassifier(
        DecisionTreeClassifier(max_features="sqrt", max_leaf_nodes=16),
        n_estimators=500, n_jobs=-1, random_state=42)

    bag_clf.fit(X_train, y_train)
    y_pred_bag = bag_clf.predict(X_test)
    np.all(y_pred_bag == y_pred_rf)
    print(np.all(y_pred_bag == y_pred_rf))


def stacking_classifier(features, labels):
    X_train, X_test, y_train, y_test = train_test_split(features, labels, random_state=42)
    stacking_clf = StackingClassifier(
        estimators=[
            ('lr', LogisticRegression(random_state=42)),
            ('rf', RandomForestClassifier(random_state=42)),
            ('svc', SVC(probability=True, random_state=42))
        ],
        final_estimator=RandomForestClassifier(random_state=43),
        cv=5  # number of cross-validation folds
    )
    stacking_clf.fit(X_train, y_train)
    print(stacking_clf.score(X_test, y_test))


def feature_selection(features, labels):
    X_train, X_test, y_train, y_test = train_test_split(features, labels, random_state=42)
    rnd_clf = RandomForestClassifier(n_estimators=500, random_state=42)
    rnd_clf.fit(features, labels)
    for score, name in zip(rnd_clf.feature_importances_, features.columns):
        print(round(score, 2), name)
