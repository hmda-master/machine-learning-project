from src.data_process.data_process import read_file
from src.data_process.feature_selector import select_mutual_information, select_relief
from src.metaclassifier.models import voting_classifier, bagging_pasting, meta_classifier_random_forest, \
    stacking_classifier, feature_selection
from src.probabilistic.bayes_models import tan, naive_bayes_model, ban_model, fan_model
from src.probabilistic.models import logic_regression, lda_model

if __name__ == '__main__':
    y_, x_ = read_file('../../data/brca_metabric_clinical_data.csv')

    naive_bayes_model(x_, y_)
    tan(x_, y_)
    fan_model(x_, y_)
    # ban_model(x_, y_)

    '''
    logic_regression(x_, y_)
    lda_model(x_, y_)
    '''

    # voting_classifier(x_, y_)
    # meta_classifier_random_forest(x_, y_)
    # stacking_classifier(x_, y_)
    # feature_selection(x_, y_)
