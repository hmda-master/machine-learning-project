from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import bartlett
import seaborn as sns


def choose_lda_configuration(X, y):
    grupo_1 = y[y['target'] == 1].drop('target', axis=1)
    grupo_0 = y[y['target'] == 0].drop('target', axis=1)

    covarianza_grupo_1 = np.cov(grupo_1, rowvar=False)
    covarianza_grupo_0 = np.cov(grupo_0, rowvar=False)

    statistic, p_value = bartlett(covarianza_grupo_1, covarianza_grupo_0)

    print(f"Estadístico de prueba de Bartlett: {statistic}")
    print(f"Valor p: {p_value}")

    if p_value < 0.05:
        print("\nSe rechaza la hipótesis nula de igualdad de matrices de covarianza.")
        return "Equal covariance matrices"
    else:
        print("\nNo hay evidencia suficiente para rechazar la hipótesis nula.")
        return "Equal spherical covariance matrices"


def plot_logistic_function(X, y, model):
    plt.figure(figsize=(8, 6))
    plt.scatter(X, y, color='black', zorder=20)

    X_test = np.linspace(X.min(), X.max(), 300)

    probabilities = model.predict_proba(X_test.reshape(-1, 1))[:, 1]

    plt.plot(X_test, probabilities, color='blue', linewidth=3)
    plt.title('Relación entre $\pi$ y $x$ en Regresión Logística')
    plt.xlabel('$x$')
    plt.ylabel('$\pi(x)$')
    plt.axhline(y=0.5, color='red', linestyle='--', label='Línea de decisión (umbral 0.5)')
    plt.legend()
    plt.show()


def logic_regression(features, labels):
    X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=42)
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    model = LogisticRegression()
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    report = classification_report(y_test, y_pred)

    print(f'Exactitud (Accuracy): {accuracy}')
    print('Informe de clasificación:\n', report)

    coeficientes = model.coef_[0]
    odds_ratio = np.exp(coeficientes)
    logit_odds = np.log(odds_ratio)
    coeficientes_df = pd.DataFrame(
        {'Variable': features.columns, 'Coeficiente': coeficientes, 'Logit Odds': logit_odds})
    coeficientes_df = coeficientes_df.sort_values(by='Logit Odds', ascending=False)
    print('Coeficientes y Logit Odds:\n', coeficientes_df)

    for index, row in coeficientes_df.iterrows():
        variable = row['Variable']
        coeficiente = row['Coeficiente']
        logit_odds = row['Logit Odds']

        if coeficiente > 0:
            print(
                f"Un aumento de 1 unidad en {variable} está asociado con un aumento de logit odds de {logit_odds:.2f}.")
        else:
            print(
                f"Un aumento de 1 unidad en {variable} está asociado con una disminución de logit odds de {-logit_odds:.2f}.")


def lda_model(features, labels):
    X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=42)

    lda = LinearDiscriminantAnalysis(solver='lsqr')
    lda.fit(X_train, y_train)

    accuracy = lda.score(X_test, y_test)
    print(f"\nPrecisión del clasificador LDA en datos de prueba: {accuracy}")

    coeficientes_discriminantes = lda.coef_[0]
    variables = features.columns

    plt.figure(figsize=(10, 6))
    plt.bar(variables, coeficientes_discriminantes,
            color=['skyblue' if c > 0 else 'lightcoral' for c in coeficientes_discriminantes])
    plt.xlabel('Variables')
    plt.ylabel('Coeficientes Discriminantes')
    plt.title('Coeficientes Discriminantes en LDA')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    plt.show()

    covarianzas = lda.covariance_

    plt.figure(figsize=(10, 8))
    sns.heatmap(covarianzas, annot=True, cmap='coolwarm', xticklabels=variables, yticklabels=variables, linewidths=0.5)
    plt.title('Matriz de Covarianza en LDA')
    plt.tight_layout()
    plt.show()



