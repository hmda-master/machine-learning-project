import networkx as nx
from pgmpy.estimators import ParameterEstimator
from pgmpy.inference import VariableElimination, CausalInference
from pgmpy.models import NaiveBayes, BayesianModel
from sklearn.model_selection import train_test_split, cross_val_score
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from src.probabilistic.bayes_utils import predict, accuracy, precision, recall, f_measure
from src.probabilistic.estimators import TreeAugmentedNaiveBayesSearch, BNAugmentedNaiveBayesSearch, \
    ForestAugmentedNaiveBayesSearch


def plot_node_relation(model, node):
    G = nx.DiGraph(model)

    plt.figure(figsize=(10, 6))
    pos = nx.spring_layout(G)

    # Agregar nodos
    nx.draw_networkx_nodes(G, pos, node_size=800, node_color="skyblue")

    # Agregar aristas
    nx.draw_networkx_edges(G, pos, width=1.0, alpha=0.5, arrowsize=20, edge_color="gray")

    # Agregar etiquetas de nodos
    nx.draw_networkx_labels(G, pos, font_size=12, font_color="black")

    plt.title(f"Grafo para el nodo: {node}")
    plt.show()


def interpretation(model):
    in_degree = model.in_degree()
    out_degree = model.out_degree()

    for node in model.nodes():
        print(f"Nodo: {node}")
        print(f"In-Degree: {in_degree[node]}, Out-Degree: {out_degree[node]}")

        # Mostrar nodos padres (in-degree)
        parents = list(model.predecessors(node))
        print(f"Nodos Padres (in-degree): {parents}")

        # Mostrar nodos hijos (out-degree)
        children = list(model.successors(node))
        print(f"Nodos Hijos (out-degree): {children}")

        # Mostrar la tabla de probabilidad condicional si existe
        cpd = model.get_cpds(node)
        if cpd is not None:
            print("Tabla de Probabilidad Condicional:")
            print(cpd)
        else:
            print("No hay información de la tabla de probabilidad para este nodo.")
        # plot_node_relation(model, node)

        print("-" * 30)


def discretize_feature(data, feature_name, bins):
    data[feature_name] = pd.cut(data[feature_name], bins=bins, labels=False)
    return data


def tan(features, labels):
    print('########################## TAN ##################################')
    features = discretize_feature(features, 'Age at Diagnosis', 8)

    columns_to_discretize = ['Nottingham prognostic index', 'Cohort', 'Tumor Size', 'Lymph nodes examined positive',
                             'Tumor Stage']
    for column in columns_to_discretize:
        features = discretize_feature(features, column, bins=5)

    df_combined = pd.concat([features, labels], axis=1)
    train_data, test_data = train_test_split(df_combined, test_size=0.2, random_state=42)

    variables_names = train_data.columns.values
    target_variable = variables_names[-1]
    feature_variables = variables_names[:-1]

    tan_estimator = TreeAugmentedNaiveBayesSearch(data=train_data, class_node=target_variable)
    tan_structure = tan_estimator.estimate()
    tan_model = BayesianModel(tan_structure.edges)

    tree_check = tan_structure.copy()
    tree_check.remove_node(target_variable)
    if nx.is_tree(tree_check):
        print('The TAN model, without the target variable, is a tree.')
    else:
        print('The TAN model, without the target variable, is not a tree.')
    nx.draw_circular(tree_check, with_labels=True, arrowsize=20, node_size=1000, font_size=10, alpha=0.8)
    plt.show()
    state_names = {column_name: features[column_name].unique().tolist() for column_name in features.columns}
    tan_model.fit(train_data, state_names=state_names)
    tan_cpds = tan_model.get_cpds()
    interpretation(tan_model)
    if not tan_model.check_model():
        print('The TAN model has errors.')
    else:
        print('The TAN model has no errors.')

    nx.draw_circular(tan_model, with_labels=True, arrowsize=20, node_size=1000, font_size=10, alpha=0.8)
    plt.show()

    tan_inference = CausalInference(tan_model)
    tan_results = predict(test_data.drop(columns=['target']), tan_inference, target_variable)
    tan_mean_results = {}
    for k, v in tan_results.items():
        tan_mean_results[k] = np.nanmean(v)
    print(tan_mean_results)

    tan_accuracy = accuracy(test_data[target_variable], tan_results)
    print(f'The TAN model has {round(tan_accuracy, 3)} % accuracy over the test set.')
    tan_precision = precision(test_data[target_variable], tan_results, positive_class='>50K', negative_class='<=50K')
    print(f'The TAN model has {round(tan_precision, 3)} % precision over the test set.')
    tan_recall = recall(test_data[target_variable], tan_results, positive_class='>50K', negative_class='<=50K')
    print(f'The TAN model has {round(tan_recall, 3)} % recall over the test set.')
    tan_f_score = f_measure(test_data[target_variable], tan_results, positive_class='>50K', negative_class='<=50K')
    print(f'The TAN model has {round(tan_f_score, 3)} % F-score over the test set.')


def visualize_bayesian_model(model):
    pos = nx.spring_layout(model)
    nx.draw(model, pos, with_labels=True, node_size=700, font_size=10, node_color='skyblue', font_color='black',
            font_weight='bold', arrowsize=20)
    plt.show()


def naive_bayes_model(features, labels):
    print('########################## naive_bayes_model ##################################')

    features = discretize_feature(features, 'Age at Diagnosis', 8)

    columns_to_discretize = ['Nottingham prognostic index', 'Cohort', 'Tumor Size', 'Lymph nodes examined positive',
                             'Tumor Stage']
    for column in columns_to_discretize:
        features = discretize_feature(features, column, bins=5)

    df_combined = pd.concat([features, labels], axis=1)
    train_data, test_data = train_test_split(df_combined, test_size=0.2, random_state=42)

    variables_names = train_data.columns.values
    target_variable = variables_names[-1]
    feature_variables = variables_names[:-1]

    model = NaiveBayes(
        feature_vars=feature_variables,
        dependent_var=target_variable
    )
    model.fit(train_data)

    interpretation(model)
    if not model.check_model():
        print('The NB model has errors.')
    else:
        print('The NB model has no errors.')

    '''
    cpd_estimator = ParameterEstimator(model, train_data)
    model = BayesianModel(cpd_estimator.get_parameters())
    visualize_bayesian_model(model)
    '''
    # model_to_graphviz = model.to_graphviz()
    # plt.show()
    # model_to_graphviz.render('naive_bayes_model', format='png', cleanup=True)
    # model_to_graphviz.view('naive_bayes_model')

    pos = nx.circular_layout(model)
    nx.draw(model, pos, with_labels=True, font_weight='bold', node_size=2000, node_color="skyblue", font_size=10,
            font_color="black")
    plt.show()

    nb_inference = CausalInference(model)
    nb_results = predict(
        pd.DataFrame(test_data.drop(columns=['target']), columns=feature_variables), nb_inference,
        target_variable)
    nb_mean_results = {}
    for k, v in nb_results.items():
        nb_mean_results[k] = np.nanmean(v)
    print(nb_mean_results)
    nb_accuracy = accuracy(test_data[target_variable], nb_results)
    print(f'The Naive Bayes model has {round(nb_accuracy, 3)} % accuracy over the test set.')
    nb_precision = precision(test_data[target_variable], nb_results, positive_class='>50K', negative_class='<=50K')
    print(f'The Naive Bayes model has {round(nb_precision, 3)} % precision over the test set.')
    nb_recall = recall(test_data[target_variable], nb_results, positive_class='>50K', negative_class='<=50K')
    print(f'The Naive Bayes model has {round(nb_recall, 3)} % recall over the test set.')
    nb_f_score = f_measure(test_data[target_variable], nb_results, positive_class='>50K', negative_class='<=50K')
    print(f'The Naive Bayes model has {round(nb_f_score, 3)} % F-score over the test set.')


def ban_model(features, labels):
    print('########################## ban_model ##################################')

    features = discretize_feature(features, 'Age at Diagnosis', 8)

    columns_to_discretize = ['Nottingham prognostic index', 'Cohort', 'Tumor Size', 'Lymph nodes examined positive',
                             'Tumor Stage']
    for column in columns_to_discretize:
        features = discretize_feature(features, column, bins=5)

    df_combined = pd.concat([features, labels], axis=1)
    train_data, test_data = train_test_split(df_combined, test_size=0.2, random_state=42)

    variables_names = train_data.columns.values
    target_variable = variables_names[-1]
    feature_variables = variables_names[:-1]

    ban_estimator = BNAugmentedNaiveBayesSearch(data=df_combined, class_node=target_variable)
    ban_structure = ban_estimator.estimate()
    ban_model = BayesianModel(ban_structure.edges)
    nx.draw_circular(ban_model, with_labels=True, arrowsize=20, node_size=1000, font_size=10, alpha=0.8)
    plt.show()
    tree_check = ban_structure.copy()
    tree_check.remove_node(target_variable)
    if nx.is_tree(tree_check):
        print('The BAN model, without the target variable, is a tree.')
    else:
        print('The BAN model, without the target variable, is not a tree.')
    nx.draw_circular(tree_check, with_labels=True, arrowsize=20, node_size=1000, font_size=10, alpha=0.8)
    state_names = {column_name: features[column_name].unique().tolist() for column_name in features.columns}

    ban_model.fit(train_data, state_names=state_names)
    ban_cpds = ban_model.get_cpds()
    if not ban_model.check_model():
        print('The BAN model has errors.')
    else:
        print('The BAN model has no errors.')

    ban_inference = VariableElimination(ban_model)
    ban_results = predict(test_data.drop(columns=['target']), ban_inference, target_variable)
    ban_mean_results = {}
    for k, v in ban_results.items():
        ban_mean_results[k] = np.nanmean(v)
    print(ban_mean_results)

    ban_accuracy = accuracy(test_data[target_variable], ban_results)
    print(f'The BAN model has {round(ban_accuracy, 3)} % accuracy over the test set.')
    ban_precision = precision(test_data[target_variable], ban_results, positive_class='>50K', negative_class='<=50K')
    print(f'The BAN model has {round(ban_precision, 3)} % precision over the test set.')
    ban_recall = recall(test_data[target_variable], ban_results, positive_class='>50K', negative_class='<=50K')
    print(f'The BAN model has {round(ban_recall, 3)} % recall over the test set.')
    ban_f_score = f_measure(test_data[target_variable], ban_results, positive_class='>50K', negative_class='<=50K')
    print(f'The BAN model has {round(ban_f_score, 3)} % F-score over the test set.')


def fan_model(features, labels):
    print('########################## fan_model ##################################')

    features = discretize_feature(features, 'Age at Diagnosis', 8)

    columns_to_discretize = ['Nottingham prognostic index', 'Cohort', 'Tumor Size', 'Lymph nodes examined positive',
                             'Tumor Stage']
    for column in columns_to_discretize:
        features = discretize_feature(features, column, bins=5)

    df_combined = pd.concat([features, labels], axis=1)
    train_data, test_data = train_test_split(df_combined, test_size=0.2, random_state=42)

    variables_names = train_data.columns.values
    target_variable = variables_names[-1]
    feature_variables = variables_names[:-1]

    fan_estimator = ForestAugmentedNaiveBayesSearch(data=train_data, class_node=target_variable)
    fan_structure = fan_estimator.estimate()
    fan_model = BayesianModel(fan_structure.edges)
    nx.draw_circular(fan_model, with_labels=True, arrowsize=20, node_size=1000, font_size=10, alpha=0.8)
    plt.show()

    forest_check = fan_structure.copy()
    forest_check.remove_node(target_variable)
    if nx.is_forest(forest_check):
        print('The FAN model, without the target variable, is a forest.')
    else:
        print('The FAN model, without the target variable, is not a forest.')
    nx.draw_circular(forest_check, with_labels=True, arrowsize=20, node_size=1000, font_size=10, alpha=0.8)
    state_names = {column_name: features[column_name].unique().tolist() for column_name in features.columns}

    fan_model.fit(train_data, state_names=state_names)
    interpretation(fan_model)
    if not fan_model.check_model():
        print('The FAN model has errors.')
    else:
        print('The FAN model has no errors.')

    fan_inference = VariableElimination(fan_model)
    fan_results = predict(test_data.drop(columns=['target']), fan_inference, target_variable)
    fan_mean_results = {}
    for k, v in fan_results.items():
        fan_mean_results[k] = np.nanmean(v)
    print(fan_mean_results)

    fan_accuracy = accuracy(test_data[target_variable], fan_results)
    print(f'The FAN model has {round(fan_accuracy, 3)} % accuracy over the test set.')
    fan_precision = precision(test_data[target_variable], fan_results, positive_class='>50K', negative_class='<=50K')
    print(f'The FAN model has {round(fan_precision, 3)} % precision over the test set.')
    fan_recall = recall(test_data[target_variable], fan_results, positive_class='>50K', negative_class='<=50K')
    print(f'The FAN model has {round(fan_recall, 3)} % recall over the test set.')
    fan_f_score = f_measure(test_data[target_variable], fan_results, positive_class='>50K', negative_class='<=50K')
    print(f'The FAN model has {round(fan_f_score, 3)} % F-score over the test set.')
