import networkx as nx
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tabulate import tabulate
from collections import defaultdict


def predict(data, inf, target_variable):
    '''
    Given a Dataframe, an inference object and a target variable,
    perform prediction and return the obtained results
    '''
    results = defaultdict(list)
    for _, data_point in data.iterrows():
        if 'index' in data_point:
            del data_point['index']
        result = inf.query(
            variables=[target_variable],
            evidence=data_point.to_dict(),
            show_progress=False,
        )
        values = result.state_names[target_variable]
        for i, val in enumerate(values):
            results[val].append(result.values[i])
    return results


def accuracy(test_data, predicted_data):
    '''
    Return a percentage representing the classification accuracy
    over the test set
    '''
    predicted_column = []
    keys = list(predicted_data.keys())
    for val in zip(*predicted_data.values()):
        max_index = np.argmax(val)
        predicted_column.append(keys[max_index])
    predicted_column = pd.Series(predicted_column, index=test_data.index)
    equality = predicted_column.eq(test_data)
    true_equality = equality[equality == True]
    return (len(true_equality) / len(test_data)) * 100


def perf_measure(test_data, predicted_data):
    '''
    Return the number of true positives, false positives, true negatives, and
    false negatives
    '''
    tp, fp, tn, fn = 0, 0, 0, 0
    predicted_column = []
    keys = list(predicted_data.keys())
    for val in zip(*predicted_data.values()):
        max_index = np.argmax(val)
        predicted_column.append(keys[max_index])
    predicted_column = pd.Series(predicted_column, index=test_data.index)
    for actual, pred in zip(test_data, predicted_column):
        if actual == pred:
            if actual == 1:  # Verdadero positivo
                tp += 1
            else:  # Verdadero negativo
                tn += 1
        else:
            if pred == 1:  # Falso positivo
                fp += 1
            else:  # Falso negativo
                fn += 1

    return tp, fp, tn, fn


def precision(test_data, predicted_data, positive_class, negative_class):
    '''
    Return a percentage representing the classification precision
    over the test set
    '''
    tp, fp, _, _ = perf_measure(test_data, predicted_data)
    return (tp / (tp + fp)) * 100


def recall(test_data, predicted_data, positive_class, negative_class):
    '''
    Return a percentage representing the classification recall
    over the test set
    '''
    tp, _, _, fn = perf_measure(test_data, predicted_data)
    return (tp / (tp + fn)) * 100


def f_measure(test_data, predicted_data, positive_class, negative_class):
    '''
    Return a percentage representing the classification F-score
    over the test set
    '''
    p = precision(test_data, predicted_data, positive_class, negative_class)
    r = recall(test_data, predicted_data, positive_class, negative_class)
    return 2 * ((p * r) / (p + r))


def interpretar_modelo_naive_bayes(cpds, variables_predictoras):
    for cpd in cpds:
        if cpd.variable in variables_predictoras:
            print(f"Probabilidades condicionales para '{cpd.variable}' dada la variable objetivo:\n")

            predictor_states = cpd.state_names[cpd.variable]
            target_states = cpd.state_names[cpd.variable]

            sums = np.zeros(len(target_states))
            for i, predictor_state in enumerate(predictor_states):
                for j, target_state in enumerate(target_states):
                    sums[j] += cpd.values[i, j]

            probabilities = sums / len(predictor_states)

            print(f'P({cpd.variable} | {cpd.variable}_class): {list(zip(target_states, probabilities))}')
            print("\n---\n")


def print_interpretable_cpds(cpds):
    for cpd in cpds:
        variable = cpd.variable
        parents = cpd.variables[:-1]
        probabilities = cpd.values

        print(f"CPD for {variable} given {parents}:")

        for parent, parent_states in zip(parents, cpd.state_names[:-1]):
            print(f"{parent}: {', '.join(parent_states)} | ", end="")

        print("\n" + tabulate(probabilities, headers=cpd.state_names[variable], tablefmt='fancy_grid'))
        print('\n')


def plot_naive_bayes(model):
    nx.draw_circular(
        model, with_labels=True, arrowsize=50, node_size=300, alpha=0.3, font_weight="bold"
    )
    plt.show()


def plot_naive_bayes_with_probabilities(edges, cpds):
    G = nx.DiGraph()
    G.add_edges_from(edges)

    # Organiza los nodos en capas con el nodo clase arriba y los nodos predictores abajo
    shell_nodes = [list(G.nodes)[:-1], [list(G.nodes)[-1]]]
    pos = nx.shell_layout(G, nlist=shell_nodes)

    node_colors = {}
    for variable, cpd in zip(G.nodes, cpds):
        max_probability = cpd.values.max()
        node_colors[variable] = max_probability

    nx.draw(G, pos, with_labels=True, font_weight='bold', node_size=700, font_size=8, arrowsize=10,
            node_color=[node_colors.get(node, 0.0) for node in G.nodes], cmap=plt.cm.Blues)

    sm = plt.cm.ScalarMappable(cmap=plt.cm.Blues)
    sm.set_array([])
    cbar = plt.colorbar(sm)
    cbar.set_label('Maximum Conditional Probability')

    plt.show()
