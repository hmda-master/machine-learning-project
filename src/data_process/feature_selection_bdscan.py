import numpy as np
import pandas as pd
from sklearn.cluster import DBSCAN
from sklearn.feature_selection import mutual_info_classif
from sklearn.utils import resample
from sklearn.preprocessing import StandardScaler, MinMaxScaler


def create_bdscan_cluster(df, n_clusters):
    class_0 = df[df['target'] == 0]
    class_1 = df[df['target'] == 1]

    minority_class = class_0 if class_0.shape[0] < class_1.shape[0] else class_1
    majority_class = class_1 if class_0.shape[0] < class_1.shape[0] else class_0

    scaler = MinMaxScaler()
    data_scaled = scaler.fit_transform(pd.concat([minority_class, majority_class]))

    minority_class = pd.DataFrame(data_scaled[:len(minority_class)], columns=minority_class.columns)
    majority_class = pd.DataFrame(data_scaled[len(minority_class):], columns=majority_class.columns)

    training_sets = [
        pd.concat([minority_class, resample(majority_class, n_samples=len(minority_class), replace=True)])
        for _ in range(n_clusters)]

    min_samples = int(0.9 * len(training_sets[0]))
    dbscan = DBSCAN(eps=5, min_samples=min_samples)

    # training_sets = [scaler.fit(training_set) for training_set in training_sets]

    clustered_sets = [dbscan.fit_predict(training_set) for training_set in training_sets]

    feature_subspaces = []
    for i, cluster_labels in enumerate(clustered_sets):
        non_noise_indices = cluster_labels != -1
        # np.where(cluster_labels != -1)[0]
        relevant_data = training_sets[i].iloc[non_noise_indices, :]
        feature_subspaces.append(relevant_data)

    # feature_selection_inbalance_data(feature_subspaces)
    return feature_selection_inbalance_data(feature_subspaces)


def feature_selection_inbalance_data(feature_subspaces):
    feature_weights = {}

    for i in feature_subspaces:
        y_ = i['target']
        x_ = i.drop(["target"], axis=1)
        x_ = x_.drop(
            columns=["relapse_free_status", "survival_months", "Overall Survival Status", "relapse_free_months"])
        mutual_info_scores = mutual_info_classif(x_, y_)
        mi_df = pd.DataFrame({'Feature': x_.columns, 'Mutual Information Score': mutual_info_scores})
        mi_df_sorted = mi_df.sort_values(by='Mutual Information Score', ascending=False)

        for index, row in mi_df_sorted.iterrows():
            feature = row['Feature']
            current_score = row['Mutual Information Score']

            # Actualizar el peso de la característica
            feature_weights[feature] = feature_weights.get(feature, 0) + current_score

    # Calcular el peso normalizado para cada característica
    total_features = sum(feature_weights.values())
    feature_weights_normalized = {feature: score / total_features for feature, score in feature_weights.items()}

    # Crear DataFrame de pesos ordenados de mayor a menor
    weights_df = pd.DataFrame(list(feature_weights_normalized.items()), columns=['Feature', 'Weight'])
    weights_df = weights_df.sort_values(by='Weight', ascending=False)
    selected_features = [feature for feature, weight in feature_weights_normalized.items() if weight > 0.01]

    return selected_features
