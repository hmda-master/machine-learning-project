from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectKBest, chi2, mutual_info_classif, f_classif
import numpy as np
import pandas as pd
from ReliefF import ReliefF

from skfeature.function.similarity_based import reliefF

from skfeature.function.statistical_based.CFS import cfs

from src.utils.utils import bar_diagram, roc_curve_plt_logistRegression


def select_features_chi_square(features, labels, method, num_splits=5):
    print("###############################################################")
    chi2_scores = chi2(features, labels)
    scores = chi2_scores[0]
    chi2_df = pd.DataFrame({'Feature': features.columns, 'Chi-squared Score': scores})
    chi2_df = chi2_df.sort_values(by='Chi-squared Score', ascending=False)
    chi2_df = chi2_df.head(20)
    bar_diagram(chi2_df, 'Feature', 'Chi-squared Score', 'Chi-squared Score for Feature Selection', method, 'Chi_2')
    c_index = roc_curve_plt_logistRegression(features, labels, chi2_df, method, 'Chi_2')
    return chi2_df, c_index


def select_mutual_information(features, labels):
    mutual_info_scores = mutual_info_classif(features, labels)
    mi_df = pd.DataFrame({'Feature': features.columns, 'Mutual Information Score': mutual_info_scores})
    mi_df_sorted = mi_df.sort_values(by='Mutual Information Score', ascending=False)
    mi_df_sorted = mi_df_sorted[mi_df_sorted["Mutual Information Score"] > 0]
    # bar_diagram(mi_df_sorted, 'Feature', 'Mutual Information Score', 'Mutual Information Score for Feature Selection',
    #          method, 'mutual_information')

    return mi_df_sorted['Feature']


def select_anova(features, labels, method, num_splits=5):
    anova_info_scores, p_values = f_classif(features, labels)
    anova_df = pd.DataFrame({'Feature': features.columns, 'ANOVA Information Score': anova_info_scores})
    anova_df_sorted = anova_df.sort_values(by='ANOVA Information Score', ascending=False)
    anova_df_sorted = anova_df_sorted.head(20)
    bar_diagram(anova_df_sorted, 'Feature', 'ANOVA Information Score', 'ANOVA Information Score for Feature Selection',
                method, 'anova')
    c_index = roc_curve_plt_logistRegression(features, labels, anova_df_sorted, method, 'anova')
    return anova_df_sorted, c_index


def select_relief(features, labels):
    relief_scores = reliefF.reliefF(features.to_numpy(), labels.to_numpy())
    relief_ = pd.DataFrame({'Feature': features.columns, 'Relief Information Score': relief_scores})
    relief_ = relief_.sort_values(by='Relief Information Score', ascending=False)
    selected_features = relief_[relief_['Relief Information Score'] > 5]
    # bar_diagram(selected_features, 'Feature', 'Relief Information Score', 'RELIEF Score for Feature Selection',
    #            method, 'RELIEF')
    # c_index = roc_curve_plt_logistRegression(features, labels, selected_features, method, 'relief')
    return selected_features['Feature']


def select_cf(features, labels, method, num_splits=5):
    selected_feature_indices = cfs(features.values, labels.values)
    selected_columns = features.columns[selected_feature_indices]
    df = pd.DataFrame({'Feature': selected_columns})
    c_index = roc_curve_plt_logistRegression(features, labels, df, method, 'CF')
    return df, c_index


def select_features_random_forest(features, labels, method, num_splits=5):
    sel = RandomForestClassifier(random_state=42)
    sel.fit(features, labels)
    importances_df = pd.DataFrame({"Feature": sel.feature_names_in_, "importances": sel.feature_importances_})
    importances_df = importances_df.sort_values(by="importances", ascending=False)  ## sorting the values of importance

    print('####################################################################')
    print('Taking the variables with high importance values over a treshold')
    print('####################################################################')
    print(importances_df[importances_df["importances"] >= 0.011])
    selected_features = importances_df[importances_df['importances'] > 0.011]
    bar_diagram(selected_features, 'Feature', 'importances', 'Random Forest for Feature Selection',
                method, 'Random Forest')
    c_index = roc_curve_plt_logistRegression(features, labels, selected_features, method, 'Random Forest')
    return selected_features, c_index
