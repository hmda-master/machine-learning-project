import os

import numpy as np
from sklearn.feature_selection import chi2, mutual_info_classif, f_classif, SelectKBest, VarianceThreshold
import pandas as pd
from ReliefF import ReliefF
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import StratifiedKFold

from src.data_process.data_process import remove_zero_variance_columns
from src.data_process.feature_selector import select_features_chi_square, select_mutual_information, select_anova, \
    select_relief, select_cf, select_features_random_forest
from src.utils.utils import bar_diagram, roc_curve_plt_logistRegression, print_feature_selection_results, \
    crear_carpeta_resultados
from skfeature.function.statistical_based.CFS import cfs


def select_features_chi_square_wrapper(features, labels, method, num_splits=5):
    best_c_index = 0
    best_k = 0
    best_features = None

    max_k = min(27, len(features.columns))
    skf = StratifiedKFold(n_splits=num_splits, shuffle=True)

    for k in range(15, max_k + 1):
        c_indices = []

        for train_index, test_index in skf.split(features, labels):
            X_train, X_test = features.iloc[train_index], features.iloc[test_index]
            y_train, y_test = labels.iloc[train_index], labels.iloc[test_index]

            feature_selector = SelectKBest(score_func=chi2, k=k)
            X_train_selected = feature_selector.fit_transform(X_train, y_train)
            X_test_selected = feature_selector.transform(X_test)

            model = LogisticRegression(max_iter=2000)
            model.fit(X_train_selected, y_train)

            y_pred = model.predict_proba(X_test_selected)[:, 1]
            c_index = roc_auc_score(y_test, y_pred)
            c_indices.append(c_index)

        mean_c_index = np.mean(c_indices)

        if mean_c_index > best_c_index:
            best_c_index = mean_c_index
            best_k = k
            # best_features = feature_selector.get_support(indices=True)

    print(f"Best K for chi-scuare: {best_k}")
    chi2_scores = chi2(features, labels)
    scores = chi2_scores[0]
    chi2_df = pd.DataFrame({'Feature': features.columns, 'Chi-squared Score': scores})
    chi2_df = chi2_df.sort_values(by='Chi-squared Score', ascending=False)
    chi2_df = chi2_df.head(best_k)
    bar_diagram(chi2_df, 'Feature', 'Chi-squared Score', 'Chi-squared Score for Feature Selection', method, 'Chi_2')
    c_index = roc_curve_plt_logistRegression(features, labels, chi2_df, method, 'Chi_2')
    return chi2_df, c_index


def select_mutual_information_wrapper(features, labels, method, num_splits=5):
    best_c_index = 0
    best_k = 0
    best_features = None

    max_k = min(27, len(features.columns))
    skf = StratifiedKFold(n_splits=num_splits, shuffle=True)

    for k in range(10, max_k + 1):
        c_indices = []

        for train_index, test_index in skf.split(features, labels):
            X_train, X_test = features.iloc[train_index], features.iloc[test_index]
            y_train, y_test = labels.iloc[train_index], labels.iloc[test_index]

            feature_selector = SelectKBest(score_func=mutual_info_classif, k=k)
            X_train_selected = feature_selector.fit_transform(X_train, y_train)
            X_test_selected = feature_selector.transform(X_test)

            model = LogisticRegression(max_iter=2000)
            model.fit(X_train_selected, y_train)

            y_pred = model.predict_proba(X_test_selected)[:, 1]
            c_index = roc_auc_score(y_test, y_pred)
            c_indices.append(c_index)

        mean_c_index = np.mean(c_indices)

        if mean_c_index > best_c_index:
            best_c_index = mean_c_index
            best_k = k
            # best_features = feature_selector.get_support(indices=True)

    print(f"Best K for mutual information: {best_k}")
    mutual_info_scores = mutual_info_classif(features, labels)
    mi_df = pd.DataFrame({'Feature': features.columns, 'Mutual Information Score': mutual_info_scores})
    mi_df_sorted = mi_df.sort_values(by='Mutual Information Score', ascending=False)
    mi_df_sorted = mi_df_sorted.head(best_k)
    bar_diagram(mi_df_sorted, 'Feature', 'Mutual Information Score', 'Mutual Information Score for Feature Selection',
                method, 'mutual_information')
    c_index = roc_curve_plt_logistRegression(features, labels, mi_df_sorted, method, 'mutual_information')
    return mi_df_sorted, c_index


def select_anova_wrapper(features, labels, method, num_splits=5):
    best_c_index = 0
    best_k = 0
    best_features = None

    max_k = min(27, len(features.columns))
    skf = StratifiedKFold(n_splits=num_splits, shuffle=True)

    for k in range(10, max_k + 1):
        c_indices = []

        for train_index, test_index in skf.split(features, labels):
            X_train, X_test = features.iloc[train_index], features.iloc[test_index]
            y_train, y_test = labels.iloc[train_index], labels.iloc[test_index]

            feature_selector = SelectKBest(score_func=f_classif, k=k)
            X_train_selected = feature_selector.fit_transform(X_train, y_train)
            X_test_selected = feature_selector.transform(X_test)

            model = LogisticRegression(max_iter=2000)
            model.fit(X_train_selected, y_train)

            y_pred = model.predict_proba(X_test_selected)[:, 1]
            c_index = roc_auc_score(y_test, y_pred)
            c_indices.append(c_index)

        mean_c_index = np.mean(c_indices)

        if mean_c_index > best_c_index:
            best_c_index = mean_c_index
            best_k = k
            # best_features = feature_selector.get_support(indices=True)

    print(f"Best K for ANOVA information: {best_k}")
    anova_info_scores, p_values = f_classif(features, labels)
    anova_df = pd.DataFrame({'Feature': features.columns, 'ANOVA Information Score': anova_info_scores})
    anova_df_sorted = anova_df.sort_values(by='ANOVA Information Score', ascending=False)
    anova_df_sorted = anova_df_sorted.head(best_k)
    bar_diagram(anova_df_sorted, 'Feature', 'ANOVA Information Score', 'ANOVA Information Score for Feature Selection',
                method, 'anova')
    c_index = roc_curve_plt_logistRegression(features, labels, anova_df_sorted, method, 'anova')
    return anova_df_sorted, c_index


def select_relief_wrapper(features, labels, method, num_splits=5):
    best_c_index = 0
    best_k = 0
    best_features = None

    max_k = min(27, len(features.columns))
    skf = StratifiedKFold(n_splits=num_splits, shuffle=True)

    for k in range(10, max_k + 1):
        c_indices = []

        for train_index, test_index in skf.split(features, labels):
            X_train, X_test = features.iloc[train_index], features.iloc[test_index]
            y_train, y_test = labels.iloc[train_index], labels.iloc[test_index]
            relief = ReliefF(n_features_to_keep=k, n_neighbors=10)
            relief.fit_transform(X_train.values, y_train.values)
            pos = pd.DataFrame(relief.feature_scores.reshape(-1, 1)).sort_values(by=0, ascending=False).head(
                k).index.tolist()
            selected_features = features.columns[pos]
            X_train_selected = X_train[selected_features]
            X_test_selected = X_test[selected_features]

            model = LogisticRegression(max_iter=2000)
            model.fit(X_train_selected, y_train)

            y_pred = model.predict_proba(X_test_selected)[:, 1]
            c_index = roc_auc_score(y_test, y_pred)
            c_indices.append(c_index)

        mean_c_index = np.mean(c_indices)

        if mean_c_index > best_c_index:
            best_c_index = mean_c_index
            best_k = k
            # best_features = relief.top_features_

    print(f"Best K for RELIEF: {best_k}")
    relief = ReliefF(n_features_to_keep=best_k, n_neighbors=10)
    relief.fit_transform(features.values, labels.values)
    relief_scores = relief.feature_scores
    relief_df = pd.DataFrame({'Feature': features.columns, 'RELIEF Score': relief_scores})
    relief_df_sorted = relief_df.sort_values(by='RELIEF Score', ascending=False)
    best_relief_features = relief_df_sorted.head(best_k)
    c_index = roc_curve_plt_logistRegression(features, labels, best_relief_features, method, 'relief')

    return best_relief_features, c_index


def select_cf_wrapper(features, labels, method, num_splits=5):
    best_c_index = 0
    best_k = 0
    best_features = None
    features = remove_zero_variance_columns(features)
    max_k = min(27, len(features.columns))
    skf = StratifiedKFold(n_splits=num_splits, shuffle=True)
    for k in range(18, max_k + 1):
        c_indices = []

        for train_index, test_index in skf.split(features, labels):
            X_train, X_test = features.iloc[train_index], features.iloc[test_index]
            y_train, y_test = labels.iloc[train_index], labels.iloc[test_index]
            # Crea un umbral de varianza
            feature_selector = SelectKBest(score_func=cfs, k=k)
            try:
                X_train_selected = feature_selector.fit_transform(X_train, y_train)
                X_test_selected = feature_selector.transform(X_test)
            except ZeroDivisionError as e:
                print(f"Error en k={k}: {e}")
                continue  # Salta esta iteración si se produce una división por cero

            model = LogisticRegression(max_iter=2000)
            model.fit(X_train_selected, y_train)

            y_pred = model.predict_proba(X_test_selected)[:, 1]
            c_index = roc_auc_score(y_test, y_pred)
            c_indices.append(c_index)

        mean_c_index = np.mean(c_indices)

        if mean_c_index > best_c_index:
            best_c_index = mean_c_index
            best_k = k
            # best_features = feature_selector.get_support(indices=True)

    print(f"Best K for CF: {best_k}")
    cf_scores = cfs(features.values, labels.values)
    cf_df = pd.DataFrame({'Feature': features.columns, 'CF Score': cf_scores})
    cf_df_sorted = cf_df.sort_values(by='CF Score', ascending=False)
    mi_df_sorted = cf_df_sorted.head(best_k)
    bar_diagram(mi_df_sorted, 'Feature', 'CF Score', 'CF Score for Feature Selection',
                method, 'CF')
    c_index = roc_curve_plt_logistRegression(features, labels, mi_df_sorted, method, 'CF')
    return mi_df_sorted, c_index


'''
 selected_features = cfs(features.values, labels.values)
    selected_scores = features.columns[selected_features.columns]
    selected_df = pd.DataFrame({'Feature': selected_scores, 'CFS Information Score': None})

    # Realiza un gráfico de barras para mostrar los puntajes de importancia de las características
    bar_diagram(selected_df, 'Feature', 'CFS Information Score', 'CFS Information Score for Feature Selection')

    # Realiza la curva ROC utilizando Logistic Regression u otro clasificador de tu elección
    roc_curve_plt_logistRegression(selected_features, labels)
'''


def evaluate_feature_selection_methods(features, labels):
    results_univariate = []
    results_multivariate = []
    results_wrapper = []

    univariate_methods = {
        'Chi-Square': select_features_chi_square(features, labels, 'Univariate'),
        'Mutual Information': select_mutual_information(features, labels, 'Univariate'),
        'ANOVA': select_anova(features, labels, 'Univariate'),
    }

    multivariate_methods = {
        'ReliefF': select_relief(features, labels, 'Multivariate'),
    }

    wrapper_methods = {
        # 'Chi-Square wrapper': select_features_chi_square(features, labels, 'wrapper'),
        'Random Forest wrapper': select_features_random_forest(features, labels, 'wrapper'),
        # 'ANOVA wrapper': select_anova(features, labels, 'wrapper'),
        # 'ReliefF wrapper': select_relief(features, labels, 'wrapper'),
        # 'CFS wrapper': select_cf(features, labels, 'wrapper'),
    }

    for method_name, (selected_features, c_index) in univariate_methods.items():
        results_univariate.append({'Method': method_name, 'C-Index': c_index, 'Selected Features': selected_features, })

    for method_name, (selected_features, c_index) in multivariate_methods.items():
        results_multivariate.append(
            {'Method': method_name, 'C-Index': c_index, 'Selected Features': selected_features, })

    for method_name, (selected_features, c_index) in wrapper_methods.items():
        results_wrapper.append(
            {'Method': method_name, 'C-Index': c_index, 'Selected Features': selected_features, })

    best_univariate_method_result = max(results_univariate, key=lambda x: x['C-Index'])
    best_multivariate_method_result = max(results_multivariate, key=lambda x: x['C-Index'])
    best_wrapper_method_result = max(results_wrapper, key=lambda x: x['C-Index'])

    best_univariate_features = best_univariate_method_result['Selected Features']
    best_multivariate_features = best_multivariate_method_result['Selected Features']
    best_wrapper_method_featues = best_wrapper_method_result['Selected Features']

    print("###############################################")
    print_feature_selection_results(results_univariate)
    print("###############################################")
    print_feature_selection_results(results_multivariate)
    print("###############################################")
    print_feature_selection_results(results_wrapper)

    return {
        'Best Univariant Method': {
            'Method': best_univariate_method_result['Method'],
            'C-Index': best_univariate_method_result['C-Index'],
            'Selected Features': best_univariate_features,
            'New Features': features[best_univariate_features],
        },
        'Best Multivariant Method': {
            'Method': best_multivariate_method_result['Method'],
            'C-Index': best_multivariate_method_result['C-Index'],
            'Selected Features': best_multivariate_features,
            'New Features': features[best_multivariate_features],
        },
        'Best Wrapper Method': {
            'Method': best_wrapper_method_result['Method'],
            'C-Index': best_wrapper_method_result['C-Index'],
            'Selected Features': best_wrapper_method_featues,
            'New Features': features[best_wrapper_method_featues],
        }
    }
