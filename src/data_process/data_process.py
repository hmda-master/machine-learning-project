import pandas as pd
import numpy as np
import tensorflow as tf
from sklearn.preprocessing import LabelEncoder
from sklearn.impute import SimpleImputer

from src.data_process.feature_selection_bdscan import create_bdscan_cluster


def encode_dummies(df_, columns):
    df_encoded = pd.get_dummies(df_, columns=columns, drop_first=True)
    return df_encoded


def get_columns_to_encode(df):
    columnas_categoricas = [col for col in df.columns if (
            df[col].dtype == 'object' or df[col].dtype.name == 'category') and col != 'Overall Survival Status']
    print("Columnas categóricas:")
    print(columnas_categoricas)
    return columnas_categoricas


def encode_label(df):
    df_encoded = df.copy()
    df_encoded['Overall Survival Status'] = np.where(df_encoded['Overall Survival Status'] == "1:DECEASED", 1, 0)
    df_encoded['Relapse Free Status'] = np.where(df_encoded['Relapse Free Status'] == "1:Recurred", 1, 0)
    return df_encoded


def clean_data(df):
    categorical_imputer = SimpleImputer(strategy='most_frequent')
    df_imputed = pd.DataFrame(categorical_imputer.fit_transform(df), columns=df.columns)

    # df = df.dropna()
    '''
    imputation_values = {}
    for col in df.columns:
        mode_value = df[col].mode()[0]
        imputation_values[col] = mode_value
    df = df.fillna(imputation_values)
    '''
    return df_imputed


def get_binary_columns(df):
    print('################################### Start getting binary columns')
    binary_columns = [col for col in df.columns if df[col].nunique() == 2]
    print(binary_columns)
    print('################################### END getting binary columns')
    return binary_columns


def embedding(df, columns):
    '''
    embedding_dim = 2
    num_oov_buckets = 2
    tf_data = {col: tf.constant(df_[col].values) for col in columns}
    for column in columns:
        unique_values = df_[column].unique()
        indices = tf.range(len(unique_values), dtype=tf.int64)
        table_init = tf.lookup.KeyValueTensorInitializer(unique_values, indices)
        table = tf.lookup.StaticVocabularyTable(table_init, num_oov_buckets)
        values = tf.constant(unique_values)
        encoded_values = table.lookup(values)
        embed_init = tf.random.uniform([len(encoded_values), embedding_dim], minval=0.0, maxval=1.0)
        embedding_matrix = tf.Variable(embed_init)
        mapping_dict = {value: idx for idx, value in enumerate(unique_values)}
        df_[column] = df_[column].map(mapping_dict)
        df_[column] = tf.nn.embedding_lookup(embedding_matrix, df_[column])
        '''
    unique_value_counts = df.nunique()
    columns_with_one_unique_value = unique_value_counts[unique_value_counts == 1].index
    print("Columnas con un solo valor único:")
    print(df[columns_with_one_unique_value])
    df = df.drop(columns=columns_with_one_unique_value)
    filtered_columns = [col for col in columns if col not in columns_with_one_unique_value]
    # encoder = OneHotEncoder(categories="auto")
    # Function that transform non-numeral labels into integers.
    # dataset.ocean_proximity = encoder.fit_transform(dataset.ocean_proximity.values)
    encoder = LabelEncoder()

    for column in filtered_columns:
        labels = list(set(df[column]))
        print(labels)
        df[column] = encoder.fit_transform(df[column].values)
        names = [encoder.inverse_transform([value]) for value in range(len(labels))]  # hold the name of each class
        print(names)
        print({value: encoder.inverse_transform([value]) for value in range(len(labels))})
    return df


def encode_one_hot(df_, columns):
    print('################################### Start one hot encode')
    tf_data = {col: tf.constant(df_[col].values) for col in columns}

    for column in columns:
        unique_values = df_[column].unique()
        indices = tf.range(len(unique_values), dtype=tf.int64)
        table_init = tf.lookup.KeyValueTensorInitializer(unique_values, indices)
        num_oov_buckets = 2
        table = tf.lookup.StaticVocabularyTable(table_init, num_oov_buckets)

        values = tf.constant(unique_values)
        encoded_values = table.lookup(values)
        cat_one_hot = tf.one_hot(encoded_values, depth=len(unique_values) + num_oov_buckets)
        print(f"Valores únicos en la columna '{column}': {unique_values}")
        print(f"Valores codificados: {encoded_values.numpy()}")
        df_[column] = table.lookup(tf_data[column])
        df_[column] = df_[column].apply(lambda x: cat_one_hot.numpy()[x])
        df_[column] = df_[column].apply(lambda x: ''.join(str(int(i)) for i in x))

    return df_


def read_mrna_illumina_microarray(df_patient):
    print('#######################################')
    print('Read mrna_illumina_microarray Data')
    print('#######################################')
    df_ = pd.read_csv('../data/data_mrna_illumina_microarray.txt', delimiter='\t')
    df_ = df_.drop(["Entrez_Gene_Id"], axis=1)
    df_transposed = df_.transpose().reset_index()
    new_header = df_transposed.iloc[0]
    df_transposed.columns = new_header
    df_transposed.reset_index(drop=True, inplace=True)
    df_transposed.rename(columns={'Hugo_Symbol': 'Patient ID'}, inplace=True)
    df_transposed = df_transposed.drop(index=0)
    df_transposed = pd.merge(df_transposed, df_patient, on='Patient ID', how='inner')
    return df_transposed


def set_binary_value_mapping(df, binary_columns):
    print('################################### Start Mapping values for binary columns')
    value_mapping = {}
    for column in binary_columns:
        if column != 'Overall Survival Status' and column != 'Relapse Free Status':
            unique_values = df[column].unique()
            if len(unique_values) == 2:
                value_mapping[column] = {unique_values[0]: 0, unique_values[1]: 1}
            if len(unique_values) == 1:
                value_mapping[column] = {unique_values[0]: 1}
    print(value_mapping)
    print('################################### END Mapping values for binary columns')
    return value_mapping


def encode_binary_columns(df, binary_columns, value_mapping):
    print('################################### Start encode_binary_columns')
    df_encoded = df.copy()
    df_encoded[binary_columns] = df_encoded[binary_columns].replace(value_mapping)
    print('################################### END encode_binary_columns')
    return df_encoded


def delete_columns_data(df):
    print('################################### Start delete_columns_data')
    # df_final = df.drop(columns=["Patient ID", "Integrative Cluster", "Overall Survival (Months)", "Patient's Vital Status"])
    df_final = df.drop(columns=["Patient ID", "Patient's Vital Status", "Sex"])
    # df_final = remove_zero_variance_columns(df_final)
    print('################################### END delete_columns_data')
    return df_final


def remove_zero_variance_columns(df):
    print('################################### Start remove_zero_variance_columns')
    df_cleaned = df.copy()
    columnas_con_varianza_cero = [col for col in df_cleaned.columns if df_cleaned[col].nunique() == 1]
    print(columnas_con_varianza_cero)
    df_cleaned = df_cleaned.drop(columns=columnas_con_varianza_cero)
    print('################################### Start remove_zero_variance_columns')
    return df_cleaned


def balance_data(df):
    class_0 = df[df['target'] == 0]
    class_1 = df[df['target'] == 1]

    minority_class = class_0 if class_0.shape[0] < class_1.shape[0] else class_1
    majority_class = class_1 if class_0.shape[0] < class_1.shape[0] else class_0

    min_samples = minority_class.shape[0]
    majority_class_sampled = majority_class.sample(n=min_samples, random_state=42)

    df_balanced = pd.concat([majority_class_sampled, minority_class])

    return df_balanced


def rename_columns(df):
    df = df.rename({'Overall Survival (Months)': 'survival_months', 'Relapse Free Status (Months)':
        'relapse_free_months', 'Relapse Free Status': 'relapse_free_status'}, axis='columns')

    def func(x, y, z):
        if (y <= 12) and (z == 1):  # change for time survival
            return 1
        else:
            return 0

    df['target'] = df.apply(lambda x: func(x.survival_months, x.relapse_free_months, x.relapse_free_status), axis=1)

    return df


def em_impute(df):
    total_rows = df.shape[0]
    for i in range(0, df.shape[1]):
        n_miss = total_rows - df.iloc[:, i].dropna().shape[0]
        if n_miss == 0:
            continue
        n = total_rows - n_miss
        # print(n, n_miss)

        mean = np.sum(df.iloc[:, i].dropna().values) / (n_miss + n)
        missingvalues = np.round(mean)
        # print(mean, missingvalues)

        prev_mean = 0
        # print(df[df.columns[i]])
        while 1:
            # print((missingvalues * n_miss) / (n + n_miss))
            updated_mean = mean + (missingvalues * n_miss) / (n + n_miss)
            missingvalues = updated_mean
            mean_difference = updated_mean - prev_mean
            prev_mean = updated_mean
            # print('\nThe Current mean is: ', np.round(updated_mean, 3))
            # print('\nThe mean Difference is: ', np.round(mean_difference, 3))
            if mean_difference < 0.05:
                break

        def func(x1, m):
            if pd.isnull(x1):
                x1 = m
            return x1

        df[df.columns[i]] = df.apply(lambda x: func(x[df.columns[i]], updated_mean), axis=1)
    return df


def read_file(file_name):
    print('#######################################')
    print('Read Clinical Data')
    print('#######################################')
    df = pd.read_csv(file_name)
    # df = clean_data(df)
    df = delete_columns_data(df)
    binary_columns = get_binary_columns(df)
    value_mapping = set_binary_value_mapping(df, binary_columns)
    df = encode_binary_columns(df, binary_columns, value_mapping)
    df = encode_label(df)
    columns_to_encode = get_columns_to_encode(df)
    df = embedding(df, columns_to_encode)
    df = rename_columns(df)
    df = em_impute(df)
    num_muestras, num_caracteristicas = df.shape
    print(f"Número total de muestras: {num_muestras}")
    print(f"Número total de características: {num_caracteristicas}")

    columns = create_bdscan_cluster(df, 20)
    # df.to_csv("C:\\Users\\theoP\\Documents\\brca_metabric\\final_df_imba.csv")
    print(columns)
    df = balance_data(df)
    x_ = df
    y_ = df['target']
    x_ = x_.drop(["target"], axis=1)
    x_ = x_[columns]
    return y_, x_
