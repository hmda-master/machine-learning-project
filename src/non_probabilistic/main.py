import pandas as pd
from sklearn.preprocessing import MinMaxScaler

from src.data_process.data_process import read_file
from src.data_process.feature_selector_wrapper import evaluate_feature_selection_methods
from src.non_probabilistic.models import decision_tree_classifier, ripper_model, neurol_network, k_nearest_neighbors, \
    svn_model, model_with_feature_selection


def agregar_resultados_a_dataframe(dataframe, metodo, modelo, reporte):
    nueva_fila = {'Modelo': modelo, 'Metodo': metodo}
    for label, valor in reporte.items():
        nueva_fila[label] = valor

    dataframe.loc[len(dataframe)] = nueva_fila
    return dataframe


def evaluar_modelos(x_, y_, metodo_seleccion, modelos):
    summary_df = pd.DataFrame(columns=['Modelo', 'Metodo', 'Precision', 'Recall', 'F1-score', 'Accuracy', 'C-index'])
    for modelo in modelos:
        report = modelo['entrenar'](x_, y_, metodo_seleccion)
        summary_df = agregar_resultados_a_dataframe(summary_df, metodo_seleccion, modelo['nombre'], report)

    return summary_df


if __name__ == '__main__':
    summary_df = pd.DataFrame(columns=['Modelo', 'Metodo', 'Precision', 'Recall', 'F1-score', 'Accuracy'])

    y_, x_ = read_file('../../data/brca_metabric_clinical_data.csv')

    scaler = MinMaxScaler()
    scaled_data = scaler.fit_transform(x_)
    scaled_df = pd.DataFrame(scaled_data, columns=x_.columns)

    # Convierte los datos escalados nuevamente en un DataFrame
    print('######### cantidad de variables iniciales')
    print(len(scaled_df.columns))
    print('######### cantidad de variables iniciales')

    modelos = [
        {'nombre': 'K-Nearest Neighbors', 'entrenar': k_nearest_neighbors},
        {'nombre': 'SVM', 'entrenar': svn_model},
        {'nombre': 'Decision Tree Classifier', 'entrenar': decision_tree_classifier},
        {'nombre': 'Artificial Neural Network', 'entrenar': neurol_network},
        {'nombre': 'RIPPER', 'entrenar': ripper_model},
    ]
    '''
    summary_results_original = evaluar_modelos(scaled_df, y_, 'Original', modelos)
    summary_results_univariado = evaluar_modelos(scaled_df[best_univariate_features], y_, 'Univariado', modelos)
    best_univariate_features = results['Best Univariant Method']['Selected Features']['Feature'].to_numpy()
    

    results = evaluate_feature_selection_methods(x_, y_)
    best_multivariate_features = results['Best Multivariant Method']['Selected Features']['Feature'].to_numpy()
    summary_results_multivariado = evaluar_modelos(scaled_df[best_multivariate_features], y_, 'Multivariado', modelos)
    '''
    # best_wrapper_features = results['Best Wrapper Method']['Selected Features']['Feature'].to_numpy()
    # summary_results_wrapper = evaluar_modelos(scaled_df[best_univariate_features], y_, 'Wrapper', modelos)

    # summary_results = pd.concat( [summary_results_original, summary_results_univariado, summary_results_multivariado, summary_results_wrapper], ignore_index=True)
    # summary_results = pd.concat([summary_results_multivariado], ignore_index=True)

    # print(summary_results)
    '''
           {
               'model_type': 'knn',
               'hyperparameters': {
                   'n_neighbors': 30,
                   'weights': 'distance',
                   'p': 2
               }
           },
           
           {
            'model_type': 'svm',
            'hyperparameters': {
                'C': 100,
                'kernel': 'poly',
                'gamma': '0.1',
                'degree': '3'
            }
        },
         {
            'model_type': 'ann',
            'hyperparameters': {
                'hidden_layer_sizes': (100, 50),
                'activation': 'relu',
                'optimizer': 'adam',
                'loss': 'binary_crossentropy',
                'epochs': 900
            }
        },
           '''
    models = [
        {
            'model_type': 'svm',
            'hyperparameters': {
                'max_depth': 5,  # Especifica los hiperparámetros de Decision Tree
                'min_samples_split': 5,
                'min_samples_leaf': 2
            }
        }
    ]

    for model_config in models:
        model_type = model_config['model_type']
        hyperparameters = model_config['hyperparameters']
        model_with_feature_selection(x_, y_, model_type, hyperparameters)

    '''
    # Crear un mapa de colores para resaltar los grupos en la columna 'Metodo'
    colores = sns.color_palette("Set2", as_cmap=True)
    metodo_colormap = dict(zip(summary_results['Metodo'].unique(),
                               sns.color_palette("Set2", n_colors=len(summary_results['Metodo'].unique()))))


    # Resaltar el valor máximo de 'Accuracy' en rojo
    def color_max_red(val):
        color = 'red' if val == summary_results['Accuracy'].max() else 'black'
        return f'color: {color}'


    # Establecer el estilo de fondo basado en el método
    styled_df = summary_results.style.apply(lambda x: [f'background-color: {metodo_colormap[x["Metodo"]]}'] * len(x), axis=1)

    # Aplicar el estilo de texto para 'Accuracy'
    styled_df = styled_df.applymap(color_max_red, subset=['Accuracy'])

    fig, ax = plt.subplots(figsize=(10, 6))  # Ajusta el tamaño según sea necesario

    # Mostrar la tabla en los ejes
    tbl = table(ax, styled_df, loc='center', cellLoc='center')

    # Establecer el estilo de la tabla
    tbl.auto_set_font_size(False)
    tbl.set_fontsize(12)
    tbl.scale(1.2, 1.2)  # Ajusta el tamaño de la tabla según sea necesario

    # Ocultar ejes
    ax.axis('off')
    plt.show()
    '''
