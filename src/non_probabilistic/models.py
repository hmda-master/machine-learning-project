# K-Nearest Neighbors
import math

import numpy as np
import pandas as pd
from keras import Sequential, Input
from keras.src.layers import Dense
from keras.src.utils import plot_model
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.decomposition import PCA
from sklearn.metrics import classification_report, confusion_matrix, precision_score, recall_score, f1_score, \
    accuracy_score, roc_auc_score
from sklearn.model_selection import cross_val_score, train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.svm import SVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier, plot_tree
from wittgenstein import RIPPER
from src.utils.utils import decision_boundary_plot, confusion_matrix_plot, tree_structure, split_data, plot_dataset, \
    plot_predictions, plot_svc_decision_boundary, roc_curve_plt_model, make_confusion_matrix
import seaborn as sns
from sklearn.tree import export_graphviz
from pathlib import Path
from graphviz import Source

from tensorflow import keras


def obtain_pca(features, y_):
    pca = PCA(n_components=2)
    principal_components = pca.fit_transform(features)
    principal_df = pd.DataFrame(data=principal_components,
                                columns=['principal component 1', 'principal component 2']).values

    X_train, X_test, y_train, y_test = split_data(principal_df, y_)
    return X_train, X_test, y_train, y_test


def model_with_feature_selection(x_, y_, model_type='knn', hyperparameters=None):
    print('-----------------------------------------------------------')
    print(f'{model_type}_with_feature_selection')
    print('-----------------------------------------------------------')
    print('-----------------------------------------------------------')

    X_train, X_test, y_train, y_test = train_test_split(x_, y_, test_size=0.2, random_state=42)

    best_features = None
    best_accuracy = 0.0
    threshold_accuracy = 0.80  # La precisión deseada (91%)

    if model_type == 'knn':
        model = KNeighborsClassifier(n_neighbors=hyperparameters['n_neighbors'], weights=hyperparameters['weights'],
                                     p=hyperparameters['p'])
    elif model_type == 'svm':
        # Escalar los datos
        scaler = StandardScaler()
        X_train = pd.DataFrame(scaler.fit_transform(X_train))
        X_test = pd.DataFrame(scaler.transform(X_test))
        model = SVC(kernel='poly', C=100, gamma=0.1, degree=3)
    elif model_type == 'decision_tree':
        model = DecisionTreeClassifier(**hyperparameters)
    for num_features_to_select in range(23, X_train.shape[1] + 1):
        if model_type == 'ann':
            model = Sequential()
            model.add(Input(shape=(X_train.iloc[:, :num_features_to_select].shape[1],)))
            num_features = X_train.iloc[:, :num_features_to_select].shape[1]
            while num_features > 1:
                num_features = math.floor(num_features / 2)
                if num_features > 0:
                    model.add(Dense(num_features, activation='relu'))
            model.add(Dense(1, activation='sigmoid'))
            model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
            model.fit(X_train.iloc[:, :num_features_to_select], y_train, epochs=900)
        elif model_type == 'ripper':
            model = RIPPER(k=10)
            model.fit(X_train.iloc[:, :num_features_to_select], y_train)
            # Calcular métricas
        else:
            model.fit(X_train.iloc[:, :num_features_to_select], y_train)
        y_pred = model.predict(X_test.iloc[:, :num_features_to_select])
        accuracy = accuracy_score(y_test, y_pred)
        print(len(X_train.iloc[:, :num_features_to_select].columns))
        print(accuracy)

        if accuracy >= threshold_accuracy:
            best_features = X_train.iloc[:, :num_features_to_select]
            best_accuracy = accuracy
            break  # Detener el bucle si alcanzamos la precisión deseada

    if best_features is not None:
        selected_feature_indices = list(range(best_features.shape[1]))
        print(f"Índices de características seleccionadas: {selected_feature_indices}")
        print(
            f"Se han seleccionado {best_features.shape[1]} variables con una precisión del {best_accuracy * 100:.2f}%")

        # Calcular las métricas
        # y_pred = model.predict(X_test[:, :best_features.shape[1]])
        if model_type == 'ann':
            y_pred = model.predict(X_test.iloc[:, :num_features_to_select])
            y_pred = (y_pred > 0.5).astype(int)
        else:
            y_pred = model.predict(X_test.iloc[:, :best_features.shape[1]])
        # accuracy = accuracy_score(y_test, y_pred)
        precision = precision_score(y_test, y_pred)
        recall = recall_score(y_test, y_pred)
        f1 = f1_score(y_test, y_pred)
        # support_val = support(test_label, test_predictions)
        accuracy = accuracy_score(y_test, y_pred)
        cindex = roc_auc_score(y_test, y_pred)
        # Agrega aquí más métricas según el modelo
        metrics = {
            'Precision': precision,
            'Recall': recall,
            'F1-score': f1,
            # 'Support': support_val,
            'Accuracy': accuracy,
            'C-index': cindex
        }
        #
        confusion_matrix_plot(model, X_test.iloc[:, :best_features.shape[1]], y_test, 'K Nearest Neighbors', 'wrapper')
        print(metrics)
        # Imprime otras métricas según el modelo

    else:
        print(f"No se encontró ningún conjunto de variables que cumpla con la precisión deseada.")

    return None  # O devuelve lo que necesites


def k_nearest_neighbors(x_, y_, name):
    print('-----------------------------------------------------------')
    print('k_nearest_neighbors')
    print('-----------------------------------------------------------')
    print('-----------------------------------------------------------')
    print(name)
    print('-----------------------------------------------------------')

    X_train, X_test, y_train, y_test = obtain_pca(x_, y_)
    point_of_interest = X_train[10]
    p_values = [1, 2]
    param_grid = {'n_neighbors': list(range(1, 50)), 'weights': ['uniform', 'distance'], 'p': p_values}
    knn = KNeighborsClassifier()
    grid_search = GridSearchCV(knn, param_grid, cv=5)
    grid_search.fit(X_train, y_train)

    best_k = grid_search.best_params_['n_neighbors']
    best_weights = grid_search.best_params_['weights']
    best_p = grid_search.best_params_['p']

    print("El mejor valor para K es -> " + str(best_k))
    print("La mejor configuración de ponderación es -> " + best_weights)
    print("El mejor valor para p en la métrica Minkowski es -> " + str(best_p))

    knn = KNeighborsClassifier(n_neighbors=best_k, weights=best_weights, p=best_p)
    knn.fit(X_train, y_train)

    # Calcular la distancia al K-ésimo vecino más cercano
    distances_k = knn.kneighbors([point_of_interest], n_neighbors=best_k, return_distance=True)[0][0][best_k - 1]

    # Dibujar una circunferencia alrededor del punto de interés basada en la distancia al K-ésimo vecino
    cmap = ListedColormap(['r', 'b'])
    plt.figure(figsize=(12, 6))
    plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cmap, alpha=0.5, label='Data Points')
    plt.scatter(point_of_interest[0], point_of_interest[1], c='green', marker='o', s=100, label='Point of Interest')

    circle = plt.Circle((point_of_interest[0], point_of_interest[1]), distances_k, color='gray', fill=False,
                        linestyle='--', linewidth=2)
    plt.gca().add_patch(circle)

    # Configurar leyenda
    legend_labels = ['DECEASED', 'SURVIVAL', 'Point of Interest', 'Circle (K Neighbors)']
    legend_handles = [plt.Line2D([0], [0], marker='o', color='w', label=label, markersize=10, markerfacecolor=color) for
                      label, color in zip(legend_labels, ['r', 'b', 'g', 'gray'])]
    plt.legend(handles=legend_handles, loc='upper right')
    # Configurar límites del gráfico para enfocarse en el área alrededor del punto de interés
    plt.xlim(point_of_interest[0] - distances_k, point_of_interest[0] + distances_k)
    plt.ylim(point_of_interest[1] - distances_k, point_of_interest[1] + distances_k)
    plt.show()

    classification_report = confusion_matrix_plot(knn, X_test, y_test, 'K Nearest Neighbors', name)
    # roc_curve_plt_model(knn, X_test, y_test)
    decision_boundary_plot(knn, X_train, y_train, model='K Nearest Neighbors', method=name)
    return classification_report


def svn_model(x_, y_, name):
    print('-----------------------------------------------------------')
    print('SVM MODEL')
    print('-----------------------------------------------------------')

    X_train, X_test, y_train, y_test = obtain_pca(x_, y_)

    # Escalar los datos
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    # Escalar los datos de entrenamiento y prueba
    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)

    # svm_ = SVC(kernel="rbf", gamma=5, C=1000)
    svm_ = LinearSVC(C=100, loss="hinge", random_state=42)
    svm_.fit(X_train_scaled, y_train)

    '''
    b1 = svm_.decision_function([-scaler.mean_ / scaler.scale_])
    w1 = svm_.coef_[0] / scaler.scale_
    svm_.intercept_ = np.array([b1])
    svm_.coef_ = np.array([w1])

    # Find support vectors (LinearSVC does not do this automatically)
    t = y_train * 2 - 1
    support_vectors_idx1 = (t * (X_train.dot(w1) + b1) < 1).ravel()
    svm_.support_vectors_ = X_train[support_vectors_idx1]
    '''

    '''
    plt.plot(X_train[:, 0][y_train == 1], X_train[:, 1][y_train == 1], "g^", label="DECEASED")
    plt.plot(X_train[:, 0][y_train == 0], X_train[:, 1][y_train == 0], "bs", label="SURVIVAL")
    '''

    fig = plt.figure(figsize=(11, 6))
    ax1 = fig.add_subplot(111, projection='3d')
    #plot_3D_decision_function(X_train, y_train, ax1, w=svm_.coef_[0], b=svm_.intercept_[0])
    plt.show()

    colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
    plt.scatter(X_test_scaled[:, 0], X_test_scaled[:, 1], c=colors[svm_.predict(X_test_scaled)])
    plt.rcParams["figure.figsize"] = [10, 8]
    plt.show()

    '''
    plot_predictions(svm_, [-2, 2.5, -2, 2.5])
    plot_dataset(X_train, y_train, [-2, 2.5, -2, 2.5])
    plt.title(r"$d=3, r=1, C=5$", fontsize=18)
    '''

    '''
    plt.xlabel("DECEASED", fontsize=14)
    plt.ylabel("SURVIVAL", fontsize=14)
    plt.legend(loc="upper left", fontsize=14)
    plt.title("$C = {}$".format(svm_.C), fontsize=16)
    plt.axis([-10, 6, -10, 6])
    '''

    # Trazar los datos de entrenamiento y el hiperplano de separación
    '''plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)
    plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=plt.cm.coolwarm)
    plt.xlabel('Feature 1')
    plt.ylabel('Feature 2')
    plt.title('SVM Training Process')
    plt.show()'''
    #    roc_curve_plt_model(best_svm, X_test, y_test)
    return classification_report


def decision_tree_classifier(x_, y_, name):
    print('-----------------------------------------------------------')
    print('Decision Tree')
    print('-----------------------------------------------------------')

    X_train, X_test, y_train, y_test = train_test_split(x_, y_, test_size=0.3, random_state=42)

    param_grid = {
        'max_depth': [None, 5, 10, 20],
        'min_samples_split': [2, 5, 10],
        'min_samples_leaf': [1, 2, 4]
    }

    tree = DecisionTreeClassifier()

    grid_search = GridSearchCV(estimator=tree, param_grid=param_grid, cv=5)
    grid_search.fit(X_train, y_train)

    # Obtener los mejores hiperparámetros
    best_max_depth = grid_search.best_params_['max_depth']
    best_min_samples_split = grid_search.best_params_['min_samples_split']
    best_min_samples_leaf = grid_search.best_params_['min_samples_leaf']

    print("Mejor max_depth encontrado:", best_max_depth)
    print("Mejor min_samples_split encontrado:", best_min_samples_split)
    print("Mejor min_samples_leaf encontrado:", best_min_samples_leaf)

    # Usar los mejores hiperparámetros para entrenar el modelo
    best_tree = DecisionTreeClassifier(criterion='gini', max_depth=best_max_depth,
                                       min_samples_split=best_min_samples_split,
                                       min_samples_leaf=best_min_samples_leaf)
    best_tree.fit(x_, y_)

    classification_report = confusion_matrix_plot(best_tree, X_test, y_test, 'Decision Tree', name)
    decision_boundary_plot(best_tree, X_train, y_train, model='Decision Tree', method=name)
    tree_structure(best_tree, X_train.columns.values)
    plt.figure(figsize=(63, 40))
    plot_tree(best_tree,
              feature_names=X_train.columns,
              class_names=['SURVIVAL', 'DECESEAD'],
              filled=True,
              fontsize=10)
    plt.show()
    return classification_report


'''
def decision_tree_classifier(x_, y_, name):
    print('-----------------------------------------------------------')
    print('Decision Tree')
    print('-----------------------------------------------------------')
    tree_parameters = {
        'criterion': 'entropy',
        'max_depth': 5
    }
    X_train, X_test, y_train, y_test = train_test_split(x_, y_, test_size=0.3, random_state=42)
    tree_ = DecisionTreeClassifier(**tree_parameters).fit(X_train, y_train)
    classification_report = confusion_matrix_plot(tree_, X_test, y_test, 'Decision Tree', name)
    decision_boundary_plot(tree_, X_train, y_train, model='Decision Tree', method=name)
    tree_structure(tree_, X_train.columns.values)
    plt.figure(figsize=(63, 40))
    plot_tree(tree_,
              feature_names=X_train.columns,
              class_names=['non-poor', 'poor'],
              filled=True,
              fontsize=10)
    plt.show()
    return classification_report

'''


def neurol_network(x_, y_, name):
    print('-----------------------------------------------------------')
    print('Neural Network')
    print('-----------------------------------------------------------')

    # Define el modelo de red neuronal

    X_train, X_test, y_train, y_test = split_data(x_, y_)

    # Inicializar el modelo secuencial
    model = Sequential()

    # Capa de entrada
    model.add(Input(shape=(X_train.shape[1],)))

    # Reducción gradual de nodos en capas ocultas
    num_features = X_train.shape[1]
    while num_features > 1:
        num_features = math.floor(num_features / 2)  # Reducción a la mitad con redondeo hacia abajo
        if num_features > 0:  # Asegúrate de que num_features sea positivo antes de agregar una capa
            model.add(Dense(num_features, activation='relu'))

    # Capa de salida
    model.add(Dense(1, activation='sigmoid'))

    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    # plot_model(model, to_file='architecture.png', show_shapes=True, show_layer_names=True)

    model.fit(X_train, y_train, epochs=900)

    history = model.history
    # print(history.keys())

    acc = history.history['accuracy']
    val_acc = history.history['accuracy']
    loss = history.history['loss']

    epochs = range(1, len(acc) + 1, 1)

    plt.figure(figsize=(12, 4))
    plt.subplot(1, 2, 1)
    plt.plot(epochs, acc, 'r--', label='Training acc')
    if val_acc:
        plt.plot(epochs, val_acc, 'b', label='Validation acc')
    plt.title('Training and Validation Accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epochs')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(epochs, loss, 'r--', label='Training loss')
    plt.title('Training Loss')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.legend()
    plt.tight_layout()
    plt.show()

    test_loss, test_accuracy = model.evaluate(X_test, y_test)

    print(f'Test Accuracy: {test_accuracy}')

    # Realiza predicciones en el conjunto de prueba
    test_predictions = model.predict(X_test)
    test_predictions = (test_predictions > 0.5).astype(int)

    precision = precision_score(y_test, test_predictions)
    recall = recall_score(y_test, test_predictions)
    f1 = f1_score(y_test, test_predictions)
    # support_val = support(test_label, test_predictions)
    accuracy = accuracy_score(y_test, test_predictions)
    cindex = roc_auc_score(y_test, test_predictions)

    metrics = {
        'Precision': precision,
        'Recall': recall,
        'F1-score': f1,
        # 'Support': support_val,
        'Accuracy': accuracy,
        'C-index': cindex
    }
    # Matriz de Confusión
    cm = confusion_matrix(y_test, test_predictions)
    make_confusion_matrix(cm, sum_stats=False)  # function to make it pretty

    return metrics


def ripper_model(x_, y_, name):
    print('-----------------------------------------------------------')
    print('RIPPER')
    print('-----------------------------------------------------------')

    X_train, X_test, y_train, y_test = obtain_pca(x_, y_)

    ripper_ = RIPPER(k=10)
    ripper_.fit(X_train, y_train)
    predictions = ripper_.predict(X_test)
    # Calcular métricas
    accuracy = accuracy_score(y_test, predictions)
    precision = precision_score(y_test, predictions)
    recall = recall_score(y_test, predictions)
    f1 = f1_score(y_test, predictions)

    print(f"Accuracy: {accuracy}")
    print(f"Precision: {precision}")
    print(f"Recall: {recall}")
    print(f"F1-score: {f1}")

    metrics = {
        'Precision': precision,
        'Recall': recall,
        'F1-score': f1,
        'Accuracy': accuracy
    }
    return metrics
