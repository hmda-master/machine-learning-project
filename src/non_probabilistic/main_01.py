import pandas as pd
from sklearn.preprocessing import MinMaxScaler

from src.data_process.data_process import read_file
from src.data_process.feature_selector_wrapper import evaluate_feature_selection_methods
from src.non_probabilistic.models import k_nearest_neighbors, svn_model, decision_tree_classifier, ripper_model, \
    neurol_network
from src.utils.utils import split_data

import numpy as np
import matplotlib.pyplot as plt

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE


def agregar_resultados_a_dataframe(dataframe, metodo, modelo, reporte):
    nueva_fila = {'Modelo': modelo, 'Metodo': metodo}
    for label, valor in reporte.items():
        nueva_fila[label] = valor

    dataframe.loc[len(dataframe)] = nueva_fila
    return dataframe


def evaluar_modelos(X_train, y_train, X_test, y_test, metodo_seleccion, modelos):
    summary_df = pd.DataFrame(columns=['Modelo', 'Metodo', 'Precision', 'Recall', 'F1-score', 'Accuracy'])
    for modelo in modelos:
        report = modelo['entrenar'](X_train, y_train, X_test, y_test, metodo_seleccion)
        summary_df = agregar_resultados_a_dataframe(summary_df, metodo_seleccion, modelo['nombre'], report)

    return summary_df


def pca_analysis(X):
    pca = PCA(n_components=2)
    principal_components = pca.fit_transform(X)
    principal_df = pd.DataFrame(data=principal_components,
                                columns=['principal component 1', 'principal component 2']).values
    fig, ax = plt.subplots(figsize=(10, 8))
    fig = plt.figure(1)
    plt.clf()
    plt.scatter(principal_df[:, 0], principal_df[:, 1])
    plt.title('PCA plot')
    plt.show()
    for k in range(2, 5):
        kmeans_clustering = KMeans(n_clusters=k)
        idx = kmeans_clustering.fit_predict(X)
        fig, ax = plt.subplots(figsize=(10, 8))

        # use pca
        X_pca = principal_components

        # plot graph
        colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
        plt.scatter(X_pca[:, 0], X_pca[:, 1], c=colors[kmeans_clustering.labels_])
        plt.title('K-Means (PCA) - ' + str(k) + ' clusters')
        plt.rcParams["figure.figsize"] = [10, 8]
        plt.show()


def tsne_analysis(X):
    for k in range(2, 5):
        kmeans_clustering = KMeans( n_clusters=k)
        idx = kmeans_clustering.fit_predict(X)
        fig, ax = plt.subplots(figsize=(10, 8))

        # use t-sne
        X_tsne = TSNE(n_components=2, perplexity=40).fit_transform(X)

        # plot graph
        colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
        plt.scatter(X_tsne[:, 0], X_tsne[:, 1], c=colors[kmeans_clustering.labels_])
        plt.title('K-Means (t-SNE) - ' + str(k) + ' clusters')
        plt.rcParams["figure.figsize"] = [10, 8]
        plt.show()


if __name__ == '__main__':
    summary_df = pd.DataFrame(columns=['Modelo', 'Metodo', 'Precision', 'Recall', 'F1-score', 'Accuracy'])

    y_, x_ = read_file('../../data/brca_metabric_clinical_data.csv')

    scaler = MinMaxScaler()
    scaled_data = scaler.fit_transform(x_)
    scaled_df = pd.DataFrame(scaled_data, columns=x_.columns)

    # Visualize natural clusters before feature selection
    pca_analysis(scaled_df)
    tsne_analysis(scaled_df)

    # X_train, X_test, y_train, y_test = split_data(scaled_df, y_)
    # # Convierte los datos escalados nuevamente en un DataFrame
    # print('######### cantidad de variables iniciales')
    # print(len(x_.columns))
    # print('######### cantidad de variables iniciales')

    #
    # modelos = [
    #     {'nombre': 'K-Nearest Neighbors', 'entrenar': k_nearest_neighbors},
    #     {'nombre': 'SVM', 'entrenar': svn_model},
    #     {'nombre': 'Decision Tree Classifier', 'entrenar': decision_tree_classifier},
    # ]
    # summary_results_original = evaluar_modelos(X_train, y_train, X_test, y_test, 'Original', modelos)
    #
    # results = evaluate_feature_selection_methods(X_train, y_train)
    #
    # best_univariate_features = results['Best Univariant Method']['Selected Features']['Feature'].to_numpy()
    # summary_results_univariado = evaluar_modelos(X_train[best_univariate_features], y_train,
    #                                              X_test[best_univariate_features], y_test,
    #                                              'Univariado', modelos)
    #
    # best_multivariate_features = results['Best Multivariant Method']['Selected Features']['Feature'].to_numpy()
    # summary_results_multivariado = evaluar_modelos(X_train[best_multivariate_features], y_train,
    #                                                X_test[best_multivariate_features], y_test,
    #                                                'Multivariado', modelos)
    #
    # summary_results = pd.concat([summary_results_original, summary_results_univariado, summary_results_multivariado],
    #                             ignore_index=True)
    #
    # print(summary_results)
